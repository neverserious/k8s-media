#!/bin/bash

[ -f ../../group_environment.sh ] && source ../../group_environment.sh
[ -f environment.sh ] && source environment.sh
[ -f version.sh ] && source version.sh

image_version="${image_version:-$(curl -s https://api.github.com/repos/linuxserver/docker-deluge/releases | grep tag_name | grep -v -- '-rc' | head -1 | awk -F': ' '{print $2}' | sed 's/,//' | xargs)}"

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

if [ "$branch" == "main" ] || [ "$branch" == "(HEAD detached at origin/main)" ]; then
    export deploy_env="prd"
    ip_subnet="34"
elif [ "$branch" == "staging" ] || [ "$branch" == "(HEAD detached at origin/staging)" ]; then
    export deploy_env="qua"
    ip_subnet="33"
else
    export deploy_env="dev"
    ip_subnet="32"
fi

work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT
base=$(realpath --relative-to=${work} $PWD)

cat << EOF > ${work}/deployment.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: deluge-config
  labels:
    app: deluge
  namespace: media
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: ${config_size}Gi
---      
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: deluge
  name: deluge
  namespace: media
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deluge
  template:
    metadata:
      labels:
        app: deluge
    spec:
      containers:
      - image: linuxserver/deluge:${image_version}
        name: deluge
        volumeMounts:
        - name: deluge-config
          mountPath: /config/
        - name: torrents
          mountPath: /downloads/
        - name: torrents
          mountPath: /mnt/files/media/torrents/
        env:
        - name: PUID
          value: "${puid}"
        - name: PGID
          value: "${pgid}"
        - name: TZ
          value: ${tz}
        - name: UMASK
          value: "002"
        ports:
        - name: "deluge-tcp"
          containerPort: 61${subnet_id}
          protocol: TCP
        - name: "deluge-udp"
          containerPort: 61${subnet_id}
          protocol: UDP
        - name: "deluge-web"
          containerPort: 8112
          protocol: TCP
        - name: "deluge-daemon"
          containerPort: 58846
          protocol: TCP
      volumes:
      - name: deluge-config
        persistentVolumeClaim:
          claimName: deluge-config
      - name: torrents
        persistentVolumeClaim:
          claimName: torrents
      dnsConfig:
        options:
        - name: ndots
          value: "1"

---
apiVersion: v1
kind: Service
metadata:
  name: deluge
  namespace: media
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-${base_ip}.${ip_subnet}.${loadbal_ip}"
spec:
  selector:
    app: deluge
  allocateLoadBalancerNodePorts: false
  ports:
    - name: deluge-web
      port: 8112
      targetPort: 8112
    - name: deluge-tcp
      port: 61${ip_subnet}
      targetPort: 61${ip_subnet}
      protocol: TCP
    - name: deluge-udp
      port: 61${ip_subnet}
      targetPort: 61${ip_subnet}
      protocol: UDP
    - name: deluge-daemon
      port: 58846
      targetPort: 58846
      protocol: TCP
      

  type: LoadBalancer
  loadBalancerIP: ${base_ip}.${ip_subnet}.${loadbal_ip}
EOF

#ls -al ${work}
cd ${work}
kubeconfig="kubeconfig_${deploy_env}"
echo "${!kubeconfig}" > kubeconfig.yml
cat deployment.yml
export KUBECONFIG="kubeconfig.yml"
kubectl ${1} -f deployment.yml