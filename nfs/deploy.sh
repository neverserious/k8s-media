#!/bin/bash

[ -f ../../group_environment.sh ] && source ../../group_environment.sh
[ -f environment.sh ] && source environment.sh
volumes="torrents main"
main_size="128Gi"
torrents_size="128Gi"
loadbal_ip="203"

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

if [ "$branch" == "main" ] || [ "$branch" == "(HEAD detached at origin/main)" ]; then
    export deploy_env="prd"
    ip_subnet="34"
    main_size="4Ti"
    torrents_size="8Ti"
elif [ "$branch" == "staging" ] || [ "$branch" == "(HEAD detached at origin/staging)" ]; then
    export deploy_env="qua"
    ip_subnet="33"
else
    export deploy_env="dev"
    ip_subnet="32"
fi

work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT

cat << EOF > ${work}/deployment.yml
apiVersion: v1
kind: Namespace
metadata:
  name: media
EOF

for volume in $volumes; do
    size="${volume}_size"
    cat << EOF >> ${work}/deployment.yml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ${volume}
  namespace: media
spec:
  storageClassName: rook-cephfs
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: ${!size}
EOF
done

#ls -al ${work}
cd ${work}
kubeconfig="kubeconfig_${deploy_env}"
echo "${!kubeconfig}" > kubeconfig.yml
cat deployment.yml
export KUBECONFIG="kubeconfig.yml"
kubectl ${1} -f deployment.yml