#!/bin/bash

[ -f ../../group_environment.sh ] && source ../../group_environment.sh

emby_image_version="latest"
rar2fs_image_version="latest"
config_size="256"
puid="1000"
pgid="1000"
loadbal_ip="203"
work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT
base=$(realpath --relative-to=${work} $PWD)

cat << EOF > ${work}/deployment.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: emby-config
  labels:
    app: emby
  namespace: media
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: ${config_size}Gi
---      
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: emby
  name: emby
  namespace: media
spec:
  replicas: 1
  selector:
    matchLabels:
      app: emby
  template:
    metadata:
      labels:
        app: emby
    spec:
      containers:
      - name: emby
        image: emby/embyserver:${emby_image_version}
        livenessProbe:
          initialDelaySeconds: 4
          periodSeconds: 3
          exec:
            command:
            - /bin/sh
            - "-xc"            
            - mount | grep /mnt/rar2fs/torrents | grep rar2fs && df -h | grep /mnt/rar2fs/torrents
        volumeMounts:
        - name: emby-config
          mountPath: /config/
        - name: emby-rar2fs-torrents
          subPath: data
          mountPath: /mnt/rar2fs/torrents
        - name: emby-rar2fs-torrents
          subPath: status
          mountPath: /status/rar2fs
        env:
        - name: UID
          value: "${puid}"
        - name: GID
          value: "${pgid}"
      - name: emby-rar2fs-torrents
        image: zimme/rar2fs:${rar2fs_image_version}
        securityContext:
          privileged: true
          capabilities:
            add:
            - SYS_ADMIN
        volumeMounts:
        - name: torrents
          mountPath: /source
        - name: emby-rar2fs-torrents
          subPath: data
          mountPath: /destination
          mountPropagation: "Bidirectional"
        - name: emby-rar2fs-torrents
          subPath: status
          mountPath: /status
        readinessProbe:
          initialDelaySeconds: 5
          exec:
            command:
            - /bin/sh
            - "-xc"
            - |
              if mount | grep /destination | grep rar2fs; then
                touch /status/ready
              else
                exit 1
              fi
      volumes:
      - name: emby-config
        persistentVolumeClaim:
          claimName: emby-config
      - name: torrents
        persistentVolumeClaim:
          claimName: torrents
      - name: emby-rar2fs-torrents
        emptyDir: {}
      dnsConfig:
        options:
        - name: ndots
          value: "1"
      
---
apiVersion: v1
kind: Service
metadata:
  name: emby
  namespace: media
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-${base_ip}.${ip_subnet}.${loadbal_ip}"
spec:
  selector:
    app: emby
  allocateLoadBalancerNodePorts: false
  ports:
    - name: http
      port: 8096
      targetPort: 8096
      protocol: TCP
    - name: https
      port: 8920
      targetPort: 8920
      protocol: TCP
  type: LoadBalancer
  loadBalancerIP: ${base_ip}.${ip_subnet}.${loadbal_ip}
EOF

#ls -al ${work}
cd ${work}
kubeconfig="kubeconfig_${deploy_env}"
echo "${!kubeconfig}" > kubeconfig.yml
cat deployment.yml
export KUBECONFIG="kubeconfig.yml"
kubectl ${1} -f deployment.yml