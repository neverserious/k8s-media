#!/bin/bash

[ -f ../../group_environment.sh ] && source ../../group_environment.sh
[ -f environment.sh ] && source environment.sh

plex_image_version="${plex_image_version:-$(curl -s https://api.github.com/repos/linuxserver/docker-plex/releases | grep tag_name | grep -v -- '-rc' | head -1 | awk -F': ' '{print $2}' | sed 's/,//' | xargs)}"
rar2fs_image_version="latest"
config_size="256"
puid="1000"
pgid="1000"
loadbal_ip="203"
work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT
base=$(realpath --relative-to=${work} $PWD)

cat << EOF > ${work}/deployment.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: plex-config
  labels:
    app: plex
  namespace: media
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: ${config_size}Gi
---      
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: plex
  name: plex
  namespace: media
spec:
  replicas: 1
  selector:
    matchLabels:
      app: plex
  template:
    metadata:
      labels:
        app: plex
    spec:
      containers:
      - name: plex
        image: linuxserver/plex:${plex_image_version}
        # command: ["/bin/sh"]
        # args: ["-c", "if [ -f /status/rar2fs/ready ]; then /init; fi"]
        livenessProbe:
          initialDelaySeconds: 4
          periodSeconds: 3
          exec:
            command:
            - /bin/sh
            - "-xc"            
            - mount | grep /mnt/rar2fs/torrents | grep rar2fs && df -h | grep /mnt/rar2fs/torrents
        volumeMounts:
        - name: plex-config
          mountPath: /config/
        - name: plex-rar2fs-torrents
          subPath: data
          mountPath: /mnt/rar2fs/torrents
        - name: plex-rar2fs-torrents
          subPath: status
          mountPath: /status/rar2fs
        env:
        - name: PUID
          value: "${puid}"
        - name: PGID
          value: "${pgid}"
        - name: PLEX_CLAIM
          value: "${plex_claim}"
      - name: plex-rar2fs-torrents
        image: zimme/rar2fs:${rar2fs_image_version}
        securityContext:
          privileged: true
          capabilities:
            add:
            - SYS_ADMIN
        volumeMounts:
        - name: torrents
          mountPath: /source
        - name: plex-rar2fs-torrents
          subPath: data
          mountPath: /destination
          mountPropagation: "Bidirectional"
        - name: plex-rar2fs-torrents
          subPath: status
          mountPath: /status
        readinessProbe:
          initialDelaySeconds: 5
          exec:
            command:
            - /bin/sh
            - "-xc"
            - |
              if mount | grep /destination | grep rar2fs; then
                touch /status/ready
              else
                exit 1
              fi
      volumes:
      - name: plex-config
        persistentVolumeClaim:
          claimName: plex-config
      - name: torrents
        persistentVolumeClaim:
          claimName: torrents
      - name: plex-rar2fs-torrents
        emptyDir: {}
      dnsConfig:
        options:
        - name: ndots
          value: "1"
      
---
apiVersion: v1
kind: Service
metadata:
  name: plex
  namespace: media
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-${base_ip}.${ip_subnet}.${loadbal_ip}"
spec:
  selector:
    app: plex
  allocateLoadBalancerNodePorts: false
  ports:
    - name: http
      port: 32400
      targetPort: 32400
      protocol: TCP
  type: LoadBalancer
  loadBalancerIP: ${base_ip}.${ip_subnet}.${loadbal_ip}
EOF

#ls -al ${work}
cd ${work}
kubeconfig="kubeconfig_${deploy_env}"
echo "${!kubeconfig}" > kubeconfig.yml
cat deployment.yml
export KUBECONFIG="kubeconfig.yml"
kubectl ${1} -f deployment.yml
echo "${plex_claim}"