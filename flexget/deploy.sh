#!/bin/bash

[ -f ../../group_environment.sh ] && source ../../group_environment.sh
[ -f environment.sh ] && source environment.sh
[ -f version.sh ] && source version.sh
config_size="4"
data_size="8"
flexget_port="5050"
puid="1000"
pgid="1000"
loadbal_ip="203"

#image_version="${image_version:-$(curl -s https://api.github.com/repos/linuxserver/docker-flexget/releases | grep tag_name | grep -v -- '-rc' | head -1 | awk -F': ' '{print $2}' | sed 's/,//' | xargs)}"
image_version="3.3.35"

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

if [ "$branch" == "main" ] || [ "$branch" == "(HEAD detached at origin/main)" ]; then
    export deploy_env="prd"
    ip_subnet="34"
elif [ "$branch" == "staging" ] || [ "$branch" == "(HEAD detached at origin/staging)" ]; then
    export deploy_env="qua"
    ip_subnet="33"
else
    export deploy_env="dev"
    ip_subnet="32"
fi

work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT
base=$(realpath --relative-to=${work} $PWD)

cat << EOF > ${work}/deployment.yml
# apiVersion: v1
# kind: PersistentVolumeClaim
# metadata:
#   name: flexget-data
#   labels:
#     app: flexget
#   namespace: media
# spec:
#   storageClassName: rook-ceph-block
#   accessModes:
#   - ReadWriteOnce
#   resources:
#     requests:
#       storage: ${data_size}Gi
# ---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: flexget-config
  labels:
    app: flexget
  namespace: media
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: ${config_size}Gi
---    
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: flexget
  name: flexget
  namespace: media
spec:
  replicas: 1
  selector:
    matchLabels:
      app: flexget
  template:
    metadata:
      labels:
        app: flexget
    spec:
      containers:
      - image: wiserain/flexget:${image_version}
        name: flexget
        volumeMounts:
        - name: flexget-config
          mountPath: /config/
        # - name: flexget-data
        #   mountPath: /data
        - name: torrents
          mountPath: /mnt/files/media/torrents/
        env:
        - name: PUID
          value: "${puid}"
        - name: PGID
          value: "${pgid}"
        - name: TZ
          value: ${tz}
      volumes:
      - name: flexget-config
        persistentVolumeClaim:
          claimName: flexget-config
      # - name: flexget-data
      #   persistentVolumeClaim:
      #     claimName: flexget-data
      - name: torrents
        persistentVolumeClaim:
          claimName: torrents
      dnsConfig:
        options:
        - name: ndots
          value: "1"

---
apiVersion: v1
kind: Service
metadata:
  name: flexget
  namespace: media
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-${base_ip}.${ip_subnet}.${loadbal_ip}"
spec:
  selector:
    app: flexget
  allocateLoadBalancerNodePorts: false
  ports:
    - name: flexget
      port: ${flexget_port}
      targetPort: ${flexget_port}
  type: LoadBalancer
  loadBalancerIP: ${base_ip}.${ip_subnet}.${loadbal_ip}
EOF

#ls -al ${work}
cd ${work}
kubeconfig="kubeconfig_${deploy_env}"
echo "${!kubeconfig}" > kubeconfig.yml
cat deployment.yml
export KUBECONFIG="kubeconfig.yml"
kubectl ${1} -f deployment.yml