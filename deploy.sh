#!/bin/bash
set -e

deployments="nfs transmission deluge flexget emby plex"

base="${PWD}"
for deployment in $deployments; do
    cd "${base}/${deployment}"
    ./deploy.sh ${1}
    cd "${base}"
done
